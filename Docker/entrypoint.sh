#!/bin/sh

APP_DIR=/vueapp
HTML_DIR=/usr/share/nginx/html

# Set default deploy path if DEPLOY_PATH is missing
DEPLOY_AT="${DEPLOY_PATH:-/}"
echo "Deploying at: $DEPLOY_AT"

if [ "$DEPLOY_AT" == "/" ]
then
  SUBST_DEPLOY=""
else
  SUBST_DEPLOY=$DEPLOY_AT
fi

find $APP_DIR -type f -name *.js -exec sed -i -e "s~/DEPLOY__PATH~$SUBST_DEPLOY~g" -e "s~{{BACKEND_API}}~$BACKEND_API~g" {} +
find $APP_DIR -type f -name *.html -exec sed -i -e "s~/DEPLOY__PATH~$SUBST_DEPLOY~g" {} +
find $APP_DIR -type f -name *.css -exec sed -i -e "s~/DEPLOY__PATH~$SUBST_DEPLOY~g" {} +

if [ "$DEPLOY_AT" == "/" ]
then
  mv $APP_DIR/* $HTML_DIR/
else
  mkdir -p $HTML_DIR/$DEPLOY_AT
  cp -r $APP_DIR/* $HTML_DIR/$DEPLOY_AT
fi


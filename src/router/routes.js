const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        name: "active",
        path: "",
        component: () => import("pages/ActivePage.vue"),
      },
      {
        name: "archived",
        path: "archived",
        component: () => import("pages/ArchivedPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;

import { defineStore } from "pinia";

export const searchJid = defineStore("jid", {
  state: () => {
    return { jid: "" };
  },

  actions: {
    set(v) {
      this.jid = v;
    },
    get() {
      return this.jid;
    },
  },
});

# build stage
FROM node:lts-alpine as build-stage
RUN npm install --location=global npm@8.12.1
RUN npm install --location=global --unsafe-perm @quasar/cli @quasar/icongenie

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN quasar build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist/spa /vueapp
COPY ./Docker/nginx_templates/default.conf.template /etc/nginx/templates/default.conf.template
COPY ./Docker/entrypoint.sh /docker-entrypoint.d/90-subst-envvars-on-app.sh
RUN chmod 775 /docker-entrypoint.d/90-subst-envvars-on-app.sh
# EXPOSE 80


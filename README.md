# marqui

A UI dashboard for a modified version of the python Arq queues package (acronym standing
for **m**odified **arq** **u**ser **i**nterface).

## Description

This is a graphical representation of the internal state of the Arq package. It is
meant to be used as a read only interface of the queues and jobs that have been
submitted to Arq.

Unfortunately this dashboard does not work on the original Arq package, but on a fork of
it [marq](https://gitlab.com/stratosgear/marq/), that offers an additional list of
features. The majority of the functionality offered by `marqui` cannot be offered in a
plain Arq installation due to his lack of additional features.

## Screenshots

- See all the available queues

## Features

## Architecture

This is a Vuejs application, build with the Quasar framework. It can be deployed as a
docker container, hosted by nginx, alongside your application

## Usage

The whole application is hosted within a single container. Depending on your needs it
can be deployed in multiple configurations.

[TODO: Provide more info]

### Standalone application

The provide docker-compose.yml offers multiple commented out sections that you can
selectively activate to turn on various deployment features.

[TODO: Provide more info]
